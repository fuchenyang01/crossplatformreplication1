# Cross Platform Replication Wrapper

This is a small cross platform (and hopefully fairly general) replication wrapper (plus ~~some~~ almost no testing), most of the core code is based on [Multiplayer Game Programming: Architecting Networked Games](https://www.oreilly.com/library/view/multiplayer-game-programming/9780134034355/) - by Josh Glazer.  This project is looking to extract out the core library allowing students in the third year Multi-Player Game Development module to use it in their own games having read the relevant book chapters.

## Plan ##

Need to write this somewhere so I don't forget short term, the original replication manager repo became to complex and focused on world / object detla based updates. I want to create a simpler version based on the earlier part of the book chapter.  <br>

Plan to add to this: <br>
* Types of packet (means of identifying transmitted information) <br>
* Replication Manager <br>
  * Naive World Replication <br>
  * ~~Delta based~~ <br>
* ~~RPCs~~ <br>
* ~~MVE for streams of objects~~ <br>

## For Students ##

Be sure to clone with: ```git clone --recurse-submodules https://smu_sc_gj@bitbucket.org/smu_sc_gj/crossplatformreplication1.git```